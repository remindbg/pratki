<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReputationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reputations', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->boolean('is_negative')->default(false);
            $table->string('trackingcode')->nullable();


            $table->unsignedInteger('receiver_id')->nullable();
            $table->unsignedInteger('sender_id')->nullable();

            $table->unsignedInteger('secure_token_id')->nullable();
            $table->boolean('is_active')->default(true);
            $table->boolean('is_reported')->default(false);
            $table->boolean('is_seen')->default(false);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reputations');
    }
}
