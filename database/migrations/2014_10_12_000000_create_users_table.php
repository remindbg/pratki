<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();

            $table->string('phone')->unique();

            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();

           $table->string('avatar_path')->nullable();
            $table->string('avatar_name')->nullable();
            $table->string('email')->unique();


            $table->boolean('is_active')->default(true);
            $table->boolean('is_admin')->default(false);

            $table->text('about')->nullable();

            $table->boolean('is_merchant')->default(false);

            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
