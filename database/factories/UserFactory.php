<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {


    return [
        'name' => $faker->userName,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'about' => $faker->paragraph,
        'is_admin' => $faker->boolean(50),
        'phone' => $faker->phoneNumber,
        'is_merchant' => $faker->boolean(50),

        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('111111'), // password
        'remember_token' => Str::random(10),
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
    ];
});
