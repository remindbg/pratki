<?php


Route::get('/', 'HomeController@index')->name('home');

/*
 * auth routes
 */
Auth::routes();



Route::view('/faq', 'staticpages.faq')->name('static.faq');
Route::view('/usloviq', 'staticpages.usloviq')->name('static.usloviq');
Route::view('/contact', 'staticpages.contact')->name('static.contact');






Route::get('/blog', 'ArticleController@index')->name('articles.index');
Route::get('/blog/{id}/{slug}/', 'ArticleController@single')->name('articles.single');
Route::get('/blog/category/{id}/{slug}', 'CategoryController@single')->name('category.single');


Route::get('theme/{themename}', 'ThemeController@switchtheme')->name('theme.switch');


Route::get('users/{id}', 'UserController@single')->name('users.single');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('changepassword', 'UserController@changepassword')->name('users.changepassword');
Route::post('updatepassword', 'UserController@updatepassword')->name('users.updatepassword');
Route::get('users/{id}/edit', 'UserController@edit')->name('users.edit')->middleware('auth');
Route::post('users/{id}/update', 'UserController@update')->name('users.update')->middleware('auth');
Route::get('users/check/{id}', 'UserController@check')->name('users.check')->middleware('auth');
Route::post('users/checkpost/{id}', 'UserController@checkpost')->name('users.checkpost')->middleware('auth');




Route::get('securitytoken/{user_id}', 'SecureTokenController@single')->name('securetoken.single')->middleware('auth');
Route::get('generatetoken', 'SecureTokenController@create')->name('securetoken.create')->middleware('auth');
Route::get('user/securetokens', 'SecureTokenController@index')->name('securetoken.index')->middleware('auth');
Route::get('user/securetokens/{id}', 'SecureTokenController@show')->name('securetoken.show')->middleware('auth');
Route::post('generatetoken/refresh/{id}', 'SecureTokenController@destroy')->name('securetoken.delete')->middleware('auth');
Route::post('generatetoken', 'SecureTokenController@store')->name('securetoken.store')->middleware('auth');




Route::get('addreputation/{receiverid}', 'ReputationController@create')->name('reputation.create')->middleware('auth');
Route::post('addreputation/check/{receiverid}', 'ReputationController@check')->name('reputation.check')->middleware('auth');
Route::get('reputationtracking/next/{receiver}', 'ReputationController@next')->name('reputation.next')->middleware('auth');
Route::post('reputationsave/{receiverid}', 'ReputationController@save')->name('reputation.save')->middleware('auth');



Route::get('reputation/{receiver}', 'ReputationController@finaldata')->name('reputation.submit')->middleware
('auth');
Route::get('viewreputation/{id}', 'ReputationController@single')->name('reputation.single');


/*
 * Search Routes
 */
Route::get('search', 'SearchController@index')->name('search.index');
Route::post('search', 'SearchController@searchpost')->name('search.searchpost');
Route::get('search/results/{results}/{searchterm}', 'SearchController@searchresults')->name('search.results');




/*
 * Admin Routes / Resource types - aka: automatic index/edit/single/delete -> dont touch if you are not well aware of
 *  the REST concept and how Laravel handles them
 * tldr: we group /panel/ routes by middleware IsAdmin, and their route name starts with admin.
 */

Route::namespace('Admin')->middleware('IsAdmin')->prefix('panel')->name('admin.')->group(function () {

    Route::view('/', 'admin.layouts.app');


    Route::resource('users','UserController');
    Route::resource('articles','ArticleController');
    Route::resource('category','CategoryController');
    Route::resource('reputations','ReputationController');
    Route::resource('userinfo','UserInfoController');

});
