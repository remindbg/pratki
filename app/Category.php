<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function articles() {
        return $this->hasMany('App\Article','category_id');
    }

    public function urlslug() {
        return url('/blog/category/'.$this->id . '/' . $this->slug);

    }
}
