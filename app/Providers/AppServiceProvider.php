<?php

namespace App\Providers;

use App\Article;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\User;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // For example im gonna locale all dates to Indonesian (ID)
        config(['app.locale' => 'bg']);
        \Carbon\Carbon::setLocale('bg');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);


        //default sidebar
        view()->composer(['_static.rightsidebar'], function
        ($view) {


            $users = User::orderBy('created_at','DESC')->limit(5)->get();
            if(Auth::check()) {
                $user = User::find(Auth::user()->id);

                $unseenreputations = $user->receivedreputations()->where('is_seen','false')->get();
            }
            else {
                $unseenreputations = collect();
            }

            $view->with('users',$users);
            $view->with('unseen',$unseenreputations)
            ;
        });

        // blog sidebar
        view()->composer(['layouts.blogapp'], function
        ($view) {

            $recentarticles = Article::with('category')->orderBy('created_at','desc')->limit(5)->get();
            $categories = Category::with('articles')->get();

            $view->with(['recentarticles' => $recentarticles, 'categories' => $categories]);

        });
    }
}
