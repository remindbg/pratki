<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','firstname','lastname','about','avatar_path','avatar_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getusername() {
        return $this->name;
    }

    public function tokens() {
        return $this->hasMany('App\SecureToken');
    }

    public  function hideemail() {
        $email = $this->email;
        $emailSplit = explode('@', $email);
        $email = $emailSplit[0];
        $len = strlen($email)-1;
        for($i = 1; $i < $len; $i++) {
            $email[$i] = '*';
        }

        $hiddenemail = $email . '@' . $emailSplit[1];

        return $hiddenemail;
    }

    public  function hidephone() {
        $phone = $this->phone;

        $result = substr($phone, 0, 3);
        $result .= "****";
        $result .= substr($phone, 7, 4);
        return $result;

    }


    public function receivedreputations() {
        return $this->hasMany('App\Reputation','receiver_id');
    }


    public function textrepcount() {

        $allreputations =  $this->receivedreputations()->where('is_active', true)->get();
        $positive = $allreputations->where('is_negative',false)->count();
        $negative =  $allreputations->where('is_negative',true)->count();



        $text = "<span class='text-success'> {$positive}</span> / <span class='text-danger'> {$negative}</span>";
        return $text;
    }


    public function ip_logins() {
        return $this->hasMany('App\UserInfo','user_id');
    }

}
