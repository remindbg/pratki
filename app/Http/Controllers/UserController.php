<?php

namespace App\Http\Controllers;

use App\SecureToken;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all()->where('is_active',true);

        return view('users.index',compact('users'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function single($id)
    {
        $user = User::find($id);


        if($user && $user->is_active = true) {

            $reputationreceived = $user->receivedreputations ?? null;


            return view('users.single',compact('user','reputationreceived'));

        }

        else {
            return back()->with('info','Потребителят не е открит');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if(!$user) {
            return redirect()->route('home')->with('error','Такъв профил не съществува');
        }

        elseif(auth()->user()->id != $id && auth()->user()->is_admin == false) {
            return redirect()->route('home')->with('error','Опитвате се да редактирате профил, който не е ваш. IDOR уязвимост не съществува в сайта :). Request IP logged');
        }
        else {

            return view('users.edit',compact('user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {

        $user = User::find($id);
        if(!$user) {
            return redirect()->route('home')->with('error','Такъв профил не съществува');
        }

        elseif(auth()->user()->id != $id) {
            return redirect()->route('home')->with('error','Опитвате се да редактирате профил, който не е ваш. IDOR уязвимост не съществува в сайта :). Request IP logged');
        }
        else {
            $data = $request->validated();


            $user->about = $data['about'];


            // lets upload avatar
            if (isset($data['avatar'])) {
                // Get image file
                $image = $data['avatar'];

                $name = $request->input('avatar') .time();

                $folder = 'avatars';
                $avatarname = $name. '.' . $image->getClientOriginalExtension();

                $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
                $ext = $image->getClientOriginalExtension();


                if($user->avatar_path) { //delete the old avatar
                    Storage::delete($user->avatar_path);
                    $user->avatar_name = null;
                }



                $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);
                Image::make( $image->getRealPath() )->fit(200, 200)->save('uploads/'. $uploaded)
                    ->destroy();

                $data['avatar'] = $uploaded;

                $user->avatar_path = $uploaded;
                $user->avatar_name = $avatarname;


            }










           $user->save();
            return redirect()->route('users.single',$user->id)->with('success','Успешна Редакция на профила');
        }
    }

    public function changepassword() {
        return view('users.changepassword');
    }




    public function updatepassword(Request $request)  {
        $user = Auth::user();

        if(strlen($request['password']) <= 5) {
            return back()->with('error','Паролата е прекалено кратка');
        }

        $user->password = $request['password'];
        $user->save();
        Auth::logout();

        return redirect()->route('home')->with('success','Успешно Сменена Парола. Моля влезте в профила си отново с новата парола');

    }

   public function check($id) {

        $user = User::find($id);

        return view('users.check',compact('user'));


   }

    public function checkpost(Request $request,$id)
    {

        $user = User::find($id);

        if (!$request['securecode']) {
            return redirect()->back()->with('error', 'Моля Въведете код');

        }
        else {
            $validatedData = $request->validate([
                'securecode' => 'required|min:100000|max:999999|integer',

            ],
                [
                    'securecode.*' => 'Неправилен формат на кода - Моля използвайте само 6 цифри'
                ]);


            if ($user->tokens()->exists()) {
                $usertokens = $user->tokens;
                $isvalid = false; // always assume the code is invalid first


                foreach ($usertokens as $token) { // foreach all securecodes of the provided user and check if its
                    // valid code or not
                    if ($token->code == $validatedData['securecode'] && $token->is_used == false) {
                        $isvalid = true;
                        break;
                    }
                }


                if ($isvalid) { //  code is 100% valid and usable for a reputation
                    $token = SecureToken::where('code', $validatedData['securecode'])->first();

                    if ($token->is_used) { //code is used/
                        return redirect()->back()->with('error', 'Кода е Неактивен или Използван Вече');
                    }
                    return redirect()
                        ->route('users.single',$user->id)
                        ->with('success','Въведеният Код е Валиден за потребител ' .$user->name);


                }



                else {  //code is valid and not used - > eligable for reputation

                    return redirect()
                        ->back()
                        ->with('error','Кода е Невалиден');
                }
            }
        }
    }
}
