<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ThemeController extends Controller
{

    public function switchtheme($themename) {
        $theme = ''; // theme filename from the directory

      switch ($themename) {
          case 'blue':
              $theme = '';
              break;
          case 'elegance':
              $theme = 'elegance.css';
              break;
          case 'pulse':
              $theme = 'pulse.css';
              break;
          case 'flat':
              $theme = 'flat.css';
              break;
          case 'corporate':
              $theme = 'corporate.css';
              break;
          case 'earth':
              $theme = 'earth.css';
              break;

          default:

      }

      return redirect()->back()->with('success','Темата е Сменена')->withCookie($theme);

    }
}
