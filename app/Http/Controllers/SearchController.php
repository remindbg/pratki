<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use function foo\func;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search.index');

    }


    public function searchpost(Request $request) {
        $searchTerm = $request['search'];
        if (!$searchTerm) {
            return back()->with('error','Моля въведете телефон или потребителско име за търсенето');
        }
        $results = User::query()
            ->where('name', 'LIKE', "%{$searchTerm}%")
            ->orWhere('phone', 'LIKE', "%{$searchTerm}%")

            ->get();

        return view('search.searchresults',compact('results','searchTerm'));

    }


    public function searchresults($results, $searchterm) {

        return view('search.searchresults');

    }
}

