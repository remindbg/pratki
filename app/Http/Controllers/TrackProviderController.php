<?php

namespace App\Http\Controllers;

use App\TrackProvider;
use Illuminate\Http\Request;

class TrackProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrackProvider  $trackProvider
     * @return \Illuminate\Http\Response
     */
    public function show(TrackProvider $trackProvider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrackProvider  $trackProvider
     * @return \Illuminate\Http\Response
     */
    public function edit(TrackProvider $trackProvider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrackProvider  $trackProvider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrackProvider $trackProvider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrackProvider  $trackProvider
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrackProvider $trackProvider)
    {
        //
    }
}
