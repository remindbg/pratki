<?php

namespace App\Http\Controllers;

use App\Reputation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\SecureToken;
use TheSeer\Tokenizer\Token;

class ReputationController extends Controller
{


    public function single($id) {

        $reputation = Reputation::find($id);
        if($reputation && $reputation->is_active) {

            if (Auth::check() && Auth::user()->id == $reputation->receiver_id) {
                $reputation->is_seen = true;
                $reputation->save();
            }

            return view('reputation.single',compact('reputation'));

        }
        else {
            return redirect('/')->with('error','Невалидна Страница');
        }


    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($receiverid)
    {
        $user = User::find($receiverid);

        if(!$user) {
            return redirect()->back()->with('error','Невалиден Потребител');
        }


        return view('reputation.next',compact('user'));
    }





    public function finaldata($receiver) {



        $receiver = User::find($receiver);

        if(!$receiver) {
            return back()->with('error','Такъв потребител не съществува');

        }

        if($receiver->is_merchant) {
            return back()->with('error','Не можете да оставяте репутация на търговци');
        }
        return view('reputation.finaldata',compact('receiver'));
    }



    public function save(Request $request,$receiverid) {

        if(! $request['trackingcode']) {
            return redirect()->back()->with('error','Моля Въведете Номер на Товарителница');

        }

        if(!$request['securecode']) {
            return redirect()->back()->with('error','Моля Въведете код');

        }
        else {
            $validatedData = $request->validate([
                'securecode' => 'required|min:100000|max:999999|integer',

            ],
                [
                    'securecode.*' => 'Неправилен формат на кода - Моля използвайте само 6 цифри'
                ]);

            $receiver = User::find($receiverid);
            if ($receiver->tokens()->exists()) {
                $usertokens = $receiver->tokens;
                $isvalid = false;
                $isused = false;

                foreach ($usertokens as $token) { // foreach all securecodes of the provided user and check if its
                    // valid code or not
                    if($token->code == $validatedData['securecode']) {
                        $isvalid = true;
                        break;
                    }
                }





                if ($isvalid) { //  code is 100% valid and usable for a reputation
                    $token = SecureToken::where('code',$validatedData['securecode'])->first();

                    if($token->is_used) {
                        return redirect()->back()->with('error','Кода е Неактивен или Използван Вече');
                    }

                    $reputation = new Reputation(); //  add a new reputation
                    if($request['is_negative']) {
                        $reputation->is_negative = true;

                    }
                    $reputation->trackingcode = $request['trackingcode'];
                    $reputation->receiver_id = $receiverid;
                    $reputation->sender_id = Auth::user()->id;

                    $token->is_used = true;
                    $token->save();


                    $reputation->secure_token_id = $token->id;


                        // assign reputation to users model
                   if ($request['is_negative'])
                   {
                       $receiver->negativecount ++;

                   }
                   else {
                       $receiver->positivecount ++;
                   }
                   $receiver->save();


                    $reputation->save();

                    return redirect()->route('users.single',$receiverid)->with('success','Успешно Добавихте Репутация на потребителя');


                }
                else { //  user have code, but its not valid
                    return redirect()->back()->with('error', 'Кода е невалиден');
                }
            } // user doesnt have any secure codes, so throw an error
            else {
                return redirect()->back()->with('error', 'Потребителят все още не е генерирал своят код за сигурност');

            }
        }

    }

}
