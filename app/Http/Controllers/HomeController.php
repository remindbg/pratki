<?php

namespace App\Http\Controllers;

use App\Reputation;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $latestreputations = Reputation::with('receiver','sender')->where('is_active',true)->limit(5)->get();
        return view('dashboard',compact('latestreputations'));
    }
}
