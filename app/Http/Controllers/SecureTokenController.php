<?php

namespace App\Http\Controllers;

use App\SecureToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class SecureTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);
        $tokens = $user->tokens;


        return view('securetokens.index',compact('user','tokens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('securetokens.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $secureToken = new SecureToken();



        $six_digit_random_number = mt_rand(100000, 999999);

        $secureToken->code = $six_digit_random_number;
        $secureToken->user_id = $user->id;
        $secureToken->save();


        return redirect()->route('securetoken.show',$secureToken->id)->with('success','Успешно Генерирахте код');


    }

    public function refresh($id) {
        $token = SecureToken::find($id);
        $user = Auth::user();
        $token->delete();
        $secureToken = new SecureToken();
        $six_digit_random_number = mt_rand(100000, 999999);

        $secureToken->code = $six_digit_random_number;
        $secureToken->user_id = $user->id;
        $secureToken->save();

        $user->secure_token_id = $secureToken->id;
        $user->save();

        return back()->with('success','Успешно Генериран Нов Код');


    }




    /**
     * Display the specified resource.
     *
     * @param  \App\SecureToken  $secureToken
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = SecureToken::find($id);

        return  view('securetokens.show',compact('token'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SecureToken  $secureToken
     * @return \Illuminate\Http\Response
     */
    public function edit(SecureToken $secureToken)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SecureToken  $secureToken
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SecureToken $secureToken)
    {



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SecureToken  $secureToken
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = SecureToken::find($id);
        $token->delete();
        return redirect()->route('securetoken.index')->with('success','Кода е успешно изтрит');
    }
}
