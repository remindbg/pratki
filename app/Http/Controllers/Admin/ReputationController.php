<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Reputation;
use Illuminate\Http\Request;

class ReputationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reputations = Reputation::with('receiver','sender','token')->paginate(10);

        return view('admin.reputations.index',compact('reputations'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reputation = Reputation::find($id);

        return view('admin.reputations.edit',compact('reputation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $reputation = Reputation::find($id);

        $reputation->trackingcode = $request['trackingcode'];

       $reputation->is_negative = $request['is_negative'];
       $reputation->is_active = $request['is_active'];

        $reputation->save();


        return redirect()->route('admin.reputations.index')->with('success','Репутацията е Редактирана');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reputation = Reputation::find($id);
        $reputation->delete();

        return redirect()->route('admin.reputations.index')->with('success','Репутацията е Изтрита');
    }
}
