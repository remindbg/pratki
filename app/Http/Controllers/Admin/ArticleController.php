<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Str;
use App\Traits\UploadTrait;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('category')->orderBy('created_at','desc')->get();

        return view('admin.article.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('admin.article.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request['category_id']) {
            return back()->with('error','Няма избрана категория!');
        }

        else {
            $article = new Article();

            $article->title = $request['title'];
            $article->slug = Str::slug($request['title'],'-');
            $article->body = $request['body'];
            if($request['is_image']) {

                if ($request->has('image')) {
                    // Get image file
                    $image = $request->file('image');
                    // Make a image name based on user name and current timestamp
                    $name = $request->input('image') .time();
                    // Define folder path
                    $folder = 'articleimages';
                    // Make a file path where image will be stored [ folder path + file name + file extension]
                    $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
                    // Upload image
                    $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);
//                $file_path = Storage::url($uploaded);
//                    $path = Storage::disk('public_uploads')->path($uploaded);



                    Image::make( $image->getRealPath() )->fit(750, 350)
                        ->save('uploads/articleimages/'. $name . '.' .$image
                                ->getClientOriginalExtension());
                    ;

                    $article->image_path = $uploaded;
                    $article->image_name = $name . '.' .$image->getClientOriginalExtension();

                }
            }
            $article->category_id = $request['category_id'];
            $article->save();

            return redirect()->route('admin.articles.index')->with('success','Статията е успешно добавена');

        }

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $categories = Category::all();

        return view('admin.article.edit',compact('article','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);

        $article->title = $request['title'];
        $article->slug = $request['slug'];
        $article->body = $request['body'];
        if($request['is_image']) {

            if ($request->has('image')) {
                // Get image file
                $image = $request->file('image');
                // Make a image name based on user name and current timestamp
                $name = $request->input('image') .time();
                // Define folder path
                $folder = 'articleimages';
                // Make a file path where image will be stored [ folder path + file name + file extension]
                $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
                // Upload image
                $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);
//                $file_path = Storage::url($uploaded);
//                    $path = Storage::disk('public_uploads')->path($uploaded);



                Image::make( $image->getRealPath() )->fit(900, 200)
                    ->save('uploads/articleimages/'. $name . '.' .$image
                            ->getClientOriginalExtension());
                ;

                $article->image_path = $uploaded;
                $article->image_name = $name . '.' .$image->getClientOriginalExtension();

            }
        }
        $article->category_id = $request['category_id'];
        $article->save();

        return redirect()->route('admin.articles.index')->with('success','Статията е успешно редактирана');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
