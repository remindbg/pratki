<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'nullable|image|max:4000',
            'about' => ['nullable','min:5','max:150'],
            'firstname' => 'nullable|string','max:45',
            'lastname' => 'nullable|string','max:45',


        ];
    }

    public function messages()
    {
        return [

            'avatar.image' => 'Неразрешен формат на аватара',
            'avatar.invalid' => 'Неразрешен формат на аватара',

            'avatar.uploaded' => 'Аватара е прекалено голям - разрешени: 4MB',
            'required' => 'Полето :attribute е задължително',

            'same'    => 'The :attribute and :other must match.',
            'size'    => 'The :attribute must be exactly :size.',
            'between' => 'The :attribute must be between :min - :max.',
            'in'      => 'The :attribute must be one of the following types: :values',

        ];
    }
}
