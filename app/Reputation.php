<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reputation extends Model
{


    public function receiver() {
        return $this->belongsTo('App\User','receiver_id');
    }

    public function sender() {
        return $this->belongsTo('App\User','sender_id');
    }

    public function token() {
        return $this->belongsTo('App\SecureToken','secure_token_id');

    }
}
