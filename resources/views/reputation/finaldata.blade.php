@extends('layouts.app')

@section('content')


    <div class="test text-center">
        <h2 class="content-heading ">
            <i class="fa fa-truck mr-5"></i> Добавяне на репутация на : <a href="{{route('users.single',
            $receiver->id)}}" target="_blank">{{$receiver->name}}</a>
        </h2>

        <div class="row">
            <div class="col-lg-12">

                <hr>

                <form action="{{route('reputation.save',$receiver)}}" method="POST">
                    @csrf

                    <div class="form-group row">
                        <label class="col-lg-5" for="example-textarea-input">Код за сигурност</label>

                        <div class="col-lg-7 col-xs-12">
                            <input type="text" min="100000" max="999999"
                                   class="form-control form-control-lg"
                                   placeholder="6 Цифрен код"
                                   name="securecode">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-5" for="trackingcode">Номер На Пратката</label>
                        <div class="col-lg-7 col-xs-12">
                            <input type="text" class="form-control form-control-lg"  id="trackingcode"
                                   value=""
                                   name="trackingcode">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-5" for="example-select">Репутация</label>
                        <div class="col-md-5 col-xs-12">
                            <select class="form-control" id="example-select" name="is_negative">
                                <option value="0">Положителна</option>
                                <option value="1">Отрицателна</option>
                            </select>
                        </div>
                    </div>

                    <hr>

                    <button class="btn btn-success btn-block btn-lg" type="submit"><i class="fa fa-arrow-right
                    mr-2"></i>Добавяне на Репутация</button>
                </form>


            </div>
        </div>
    </div>



    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
