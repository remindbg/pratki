@extends('layouts.app')


@section('seo')
    <title>Преглед на {{$reputation->is_negative ? 'отрицателна' : 'положителна'}}  Репутация</title>

    <meta name="description" content="Преглед на {{$reputation->is_negative ? 'отрицателна' : 'положителна'}}
        репутация">
@endsection


@section('content')

    <div class="test text-center">
        <h2 class="content-heading ">
            <i class="fa fa-info mr-5"></i> Преглед  на {{$reputation->is_negative ? 'отрицателна' : 'положителна'}}
            репутация от {{$reputation->created_at->diffForHumans()}}
        </h2>

        <div class="row">
            <div class="col-lg-12">
                <p class=""></p>
                <div class="block block-themed">
                    <div class="block-header {{$reputation->is_negative ? 'bg-gd-cherry' : 'bg-earth'}}">
                        <h3 class="block-title">{{$reputation->is_negative ? 'Отрицателна' : 'Положителна'}}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-info"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-lg-5"><h5 class="text-primary">Получател:</h5></div>
                            <div class="col-lg-5"><h3><a class="text-primary" target="_blank" href="{{route('users.single',
                            $reputation->receiver->id)
                            }}">{{$reputation->receiver->name}}</a></h3></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-5"><h5 class="text-primary">Дата:</h5></div>
                            <div class="col-lg-5">{{$reputation->created_at}}</div>
                        </div>




                        <hr>
                        <div class="row">
                            <div class="col-lg-5 text-primary"><p>Репутацията е изпратена от:</p></div>
                            <div class="col-lg-5">
                                <h3>
                                    <a class="text-primary under" target="_blank" href="{{route('users.single',
                            $reputation->sender->id)
                            }}">
                                   {{$reputation->sender->name}}
                                </a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
