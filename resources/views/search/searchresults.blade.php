@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="row justify-content-center">

        <div class="col-md-12 col-lg-12 col-xl-12">
            <h3 class="font-w700 text-primary mb-10 mt-100">Търсене на Потребители</h3>
            <hr>


            <form action="{{route('search.searchpost')}}" method="POST">
                @csrf
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control" placeholder="телефон или потребителско име"
                           name="search">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="block">
        <div class="block-content block-content-full overflow-hidden">


            <div class="font-size-h3 font-w600 py-30 mb-20 text-center border-b">
                <span class="text-primary font-w700">{{$results->count()}}</span> Открити резултата за <mark
                    class="text-info">{{$searchTerm}} .</mark>
            </div>
            <table class="table table-striped table-borderless table-hover table-vcenter">
                <thead class="thead-light">
                <tr>
                    <th class="text-center" style="width: 70px;"><i class="si si-user"></i></th>
                    <th>Потребителско Име</th>
                    <th class="d-none d-sm-table-cell">Имейл</th>
                    <th class="d-none d-lg-table-cell" style="width: 15%;">Телефон</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($results  as $user)
                    <tr>

                        <td class="text-center">
                            @if (!$user->avatar_path)
                                <i class="si si-user"></i>
                                @else
                                <img class="img-avatar img-avatar48" src="{{asset('uploads/'.$user->avatar_path)}}" alt="">

                            @endif
                        </td>
                        <td class="font-w600">
                            <a href="{{route('users.single',$user->id)}}">{{$user->name}}</a>
                        </td>
                        <td class="d-none d-sm-table-cell">
                           {{$user->hideemail()}}
                        </td>
                        <td class="d-none d-lg-table-cell">
                            <span class="badge badge-primary">{{$user->hidephone()}}</span>
                        </td>

                    </tr>
                @empty
                <h4>Няма Открити Резултати</h4>
                @endforelse


                </tbody>
            </table>
        </div>


    </div>


    <!-- END Page Content -->
@endsection
