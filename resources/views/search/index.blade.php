@extends('layouts.app')

@section('seo')
    <title>Търсене</title>

    <meta name="description" content="Търсене в сайта">
@endsection


@section('content')
    <!-- Page Content -->
    <div class="row justify-content-center">

        <div class="col-md-12 col-lg-12 col-xl-12">
            <h3 class="font-w700 text-primary mb-10 mt-100">Търсене на Потребители</h3>
            <hr>

            <form action="{{route('search.searchpost')}}" method="POST">
                @csrf
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control" placeholder=" потребителско име или телефон"
                           name="search">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="block">


        </div>





    <!-- END Page Content -->
@endsection
