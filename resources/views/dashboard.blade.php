@extends('layouts.app')

@section('seo')
    <title>ЗАГЛАВИЕ</title>

    <meta name="description" content="ЗАГЛАВИЕ">
@endsection

@section('content')
    <div class=" text-center">
            <h3 class="font-w700 text-primary mb-10 mt-100">Търсене на Потребители</h3>
            <hr>
            <form action="{{route('search.searchpost')}}" method="POST">
                @csrf
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control" placeholder=" потребителско име или телефон"
                           name="search">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

    </div>
        <div class="my-50 text-center">
            <h3 class="font-w700 text-primary mb-10">Последно Оставени Репутации</h3>
            <h3 class="h5 text-muted mb-0">Разгледайте последно оставените репутации</h3>
        </div>

                <div class="">
                    <div class="">
                        <table class="table table-borderless">
                            <tbody>
                            <tr class="table-active">
                                <th style="" class=" d-none d-xl-block"></th>
                                <th class="">От</th>
                                <th class="">Към</th>
                                <th>Тип</th>
                            </tr>
                            @forelse($latestreputations as $rep)
                            <tr>
                                <td class="d-none d-xl-block {{$rep->is_negative ? 'table-danger' : 'table-success'}}
                                text-center">
                                    @if ($rep->is_negative)
                                        <i class="fa fa-fw fa-frown-o text-danger"></i>
                                    @else

                                        <i class="fa fa-fw fa-smile-o text-success"></i>
                                    @endif
                                </td>
                                <td class="">
                                    <a href="{{route('users.single',$rep->sender->id)
                                    }}">{{\Illuminate\Support\Str::limit($rep->sender->name,11)}}

                                        <span class="">{!! $rep->sender->textrepcount() !!}</span></a>
                                </td>
                                <td class="">
                                    <a href="{{route('users.single',$rep->receiver->id)
                                    }}">{{\Illuminate\Support\Str::limit($rep->receiver->name,11)}}
                                        <span class="">{!! $rep->receiver->textrepcount() !!}</span>
                                    </a>

                                </td>
                                <td>
                                    @if ($rep->is_negative)
                                        <a href="{{route('reputation.single',$rep->id)}}" class="badge
                                        badge-danger">Виж</a>

                                    @else

                                        <a href="{{route('reputation.single',$rep->id)}}" class="badge
                                        badge-success">Виж</a>

                                    @endif
                                </td>
                            </tr>
                            @empty
                                <h4 class="text-primary">Няма Оставени Репутации Все още</h4>
                            @endforelse

                            </tbody>
                        </table>

                    </div>
                </div>
@endsection
