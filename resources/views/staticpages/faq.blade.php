@extends('layouts.app')

@section('content')
    <!-- Page Content -->

        <div class="my-50">
            <h2 class="font-w700 text-primary mb-10">Често Задавани Въпроси</h2>
        </div>



                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">
                             Introduction
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-question"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div id="faq1" role="tablist" aria-multiselectable="true">
                            <div class="block block-bordered block-rounded mb-5">
                                <div class="block-header" role="tab" id="faq1_h1">
                                    <a class="font-w600 text-body-color-dark collapsed" data-toggle="collapse" href="#faq1_q1" aria-expanded="false" aria-controls="faq1_q1"> Welcome to your own dashboard</a>
                                </div>
                                <div id="faq1_q1" class="collapse" role="tabpanel" aria-labelledby="faq1_h1" data-parent="#faq1" style="">
                                    <div class="block-content border-t">
                                        <p>Potenti elit lectus augue eget iaculis vitae etiam, ullamcorper etiam bibendum ad feugiat magna accumsan dolor, nibh molestie cras hac ac ad massa, fusce ante convallis ante urna molestie vulputate bibendum tempus ante justo arcu erat accumsan adipiscing risus, libero condimentum venenatis sit nisl nisi ultricies sed, fames aliquet consectetur consequat nostra molestie neque nullam scelerisque neque commodo turpis quisque etiam egestas vulputate massa, curabitur tellus massa venenatis congue dolor enim integer luctus, nisi suscipit gravida fames quis vulputate nisi viverra luctus id leo dictum lorem, inceptos nibh orci.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="block block-bordered block-rounded mb-5">
                                <div class="block-header" role="tab" id="faq1_h3">
                                    <a class="font-w600 text-body-color-dark" data-toggle="collapse" href="#faq1_q3" aria-expanded="true" aria-controls="faq1_q3"> What are our values?</a>
                                </div>
                                <div id="faq1_q3" class="collapse" role="tabpanel" aria-labelledby="faq1_h3" data-parent="#faq1" style="">
                                    <div class="block-content border-t">
                                        <p>Potenti elit lectus augue eget iaculis vitae etiam, ullamcorper etiam bibendum ad feugiat magna accumsan dolor, nibh molestie cras hac ac ad massa, fusce ante convallis ante urna molestie vulputate bibendum tempus ante justo arcu erat accumsan adipiscing risus, libero condimentum venenatis sit nisl nisi ultricies sed, fames aliquet consectetur consequat nostra molestie neque nullam scelerisque neque commodo turpis quisque etiam egestas vulputate massa, curabitur tellus massa venenatis congue dolor enim integer luctus, nisi suscipit gravida fames quis vulputate nisi viverra luctus id leo dictum lorem, inceptos nibh orci.</p>
                                        <p>Potenti elit lectus augue eget iaculis vitae etiam, ullamcorper etiam bibendum ad feugiat magna accumsan dolor, nibh molestie cras hac ac ad massa, fusce ante convallis ante urna molestie vulputate bibendum tempus ante justo arcu erat accumsan adipiscing risus, libero condimentum venenatis sit nisl nisi ultricies sed, fames aliquet consectetur consequat nostra molestie neque nullam scelerisque neque commodo turpis quisque etiam egestas vulputate massa, curabitur tellus massa venenatis congue dolor enim integer luctus, nisi suscipit gravida fames quis vulputate nisi viverra luctus id leo dictum lorem, inceptos nibh orci.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="block block-bordered block-rounded mb-5">
                                <div class="block-header" role="tab" id="faq1_h4">
                                    <a class="font-w600 text-body-color-dark" data-toggle="collapse" href="#faq1_q4" aria-expanded="true" aria-controls="faq1_q4"> What are our future plans?</a>
                                </div>
                                <div id="faq1_q4" class="collapse" role="tabpanel" aria-labelledby="faq1_h4" data-parent="#faq1">
                                    <div class="block-content border-t">
                                        <p>Potenti elit lectus augue eget iaculis vitae etiam, ullamcorper etiam bibendum ad feugiat magna accumsan dolor, nibh molestie cras hac ac ad massa, fusce ante convallis ante urna molestie vulputate bibendum tempus ante justo arcu erat accumsan adipiscing risus, libero condimentum venenatis sit nisl nisi ultricies sed, fames aliquet consectetur consequat nostra molestie neque nullam scelerisque neque commodo turpis quisque etiam egestas vulputate massa, curabitur tellus massa venenatis congue dolor enim integer luctus, nisi suscipit gravida fames quis vulputate nisi viverra luctus id leo dictum lorem, inceptos nibh orci.</p>
                                        <p>Potenti elit lectus augue eget iaculis vitae etiam, ullamcorper etiam bibendum ad feugiat magna accumsan dolor, nibh molestie cras hac ac ad massa, fusce ante convallis ante urna molestie vulputate bibendum tempus ante justo arcu erat accumsan adipiscing risus, libero condimentum venenatis sit nisl nisi ultricies sed, fames aliquet consectetur consequat nostra molestie neque nullam scelerisque neque commodo turpis quisque etiam egestas vulputate massa, curabitur tellus massa venenatis congue dolor enim integer luctus, nisi suscipit gravida fames quis vulputate nisi viverra luctus id leo dictum lorem, inceptos nibh orci.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




        <!-- END Page Content -->
        @endsection

























