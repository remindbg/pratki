@extends('layouts.app')


@section('seo')
    <title>Вход</title>

    <meta name="description" content="Вход в сайта">
@endsection



@section('content')
<div class="container-fluid">


            <div class="hero-static">

                <div class="content content-full">

                    <!-- Header -->
                    <div class="py-30 text-center">
                        <a class="link-effect font-w700" href="/login">
                            <i class="si si-fire"></i>
                            <span class="font-size-xl text-primary-dark">Check</span><span class="font-size-xl">BG</span>
                        </a>
                        <h1 class="h4 font-w700 mt-30 mb-10">Вход</h1>
                    </div>

                    <form class="js-validation-signup" action="{{route('login')}}" method="POST">
                        @csrf
                        <div class="block block-themed block-rounded block-shadow">
                            <div class="block-header bg-gd-aqua">
                                <h3 class="block-title">Потребителски Данни</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option">
                                        <i class="si si-user"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="input-group">
                                            <label for="username">Потребителско Име или Имейл</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                </div>
                                                <input type="text" class="form-control" id="username"
                                                       name="email" placeholder="">
                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="input-group">
                                            <label for="password">Парола</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-key"></i>
                                                        </span>
                                                </div>
                                                <input type="password" class="form-control" id="password"
                                                       name="password" placeholder="">
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 ">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            Запомни ме
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content bg-body-light">
                                <button class="btn btn-block btn-primary">Вход</button>
                            </div>
                        </div>
                        <div class="block-content bg-body-light">
                            <div class="form-group text-center">
                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="/usloviq"
                                   data-toggle="modal" data-target="#modal-terms">
                                    <i class="fa fa-book text-muted mr-5"></i> Условия и Правила
                                </a>
                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="/">
                                    <i class="fa fa-user text-muted mr-5"></i> Вход
                                </a>
                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route
                                ('password.request')}}">
                                    <i class="fa fa-user text-muted mr-5"></i> Забравена Парола
                                </a>
                            </div>
                        </div>
                    </form>
                    <!-- END Sign Up Form -->
                </div>
            </div>

    </div>

@endsection
