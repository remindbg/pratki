@extends('layouts.app')


@section('seo')
    <title>Регистрация</title>

    <meta name="description" content="Регистрация в сайта">
@endsection


@section('content')
<div class="container">
    <div class="row mx-0 justify-content-center">
        <div class="hero-static">

            <div class="content content-full overflow-hidden">

                <!-- Header -->
                <div class="py-30 text-center">
                    <a class="link-effect font-w700" href="/register">
                        <i class="si si-fire"></i>
                        <span class="font-size-xl text-primary-dark">Check</span><span class="font-size-xl">BG</span>
                    </a>
                    <h1 class="h4 font-w700 mt-30 mb-10">Създаване На Нов Акаунт</h1>
                    <h2 class="h5 font-w400 text-muted mb-0">Някакво малко описание за регистрацията</h2>
                </div>

                <form class="js-validation-signup" action="{{route('register')}}" method="post">
                    @csrf
                    <div class="block block-themed block-rounded block-shadow">
                        <div class="block-header bg-gd-aqua">
                            <h3 class="block-title">Потребителски Данни</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
                                <div class="col-lg-6 col-xs-12">
                                    <div class="input-group">
                                        <label for="signup-username">Потребителско Име</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                            </div>
                                            <input type="text" class="form-control" id="signup-username"
                                                   name="name" placeholder="пример: ivan_ivanov">
                                        </div>

                                    </div>
                                    <hr>

                                </div>
                                <div class="col-lg-6 col-xs-12">

                                    <label for="phone">Телефон</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                        </div>
                                        <input type="text" class="form-control" id="phone"
                                               name="phone" placeholder="пример: 0884343215">
                                    </div>
                                    <hr>

                                </div>

                                <div class="col-lg-6 col-xs-12">

                                    <label for="firstname">Име</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">

                                        </div>
                                        <input type="text" class="form-control" id="firstname"
                                               name="firstname" placeholder="пример: Иван">
                                    </div>

                                </div>

                                  <div class="col-lg-6 col-xs-12">

                                      <label for="lastname">Фамилия</label>
                                      <div class="input-group">
                                          <div class="input-group-prepend">

                                          </div>
                                          <input type="text" class="form-control" id="lastname"
                                                 name="lastname" placeholder="пример: Маринов">
                                      </div>

                                  </div>



                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="signup-email">Валиден Имейл Адрес</label>
                                    <input type="email" class="form-control" id="signup-email"
                                           name="email"
                                           placeholder="пример: ivan@checkbg.bg">
                                </div>
                                <hr>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="signup-password">Парола</label>
                                    <input type="password" class="form-control" id="signup-password" name="password"
                                           placeholder="********">
                                </div>
                                <hr>
                            </div>

                            <div class="form-group row mb-0">

                                <div class="col-sm-12 text-sm-left push">
                                    <div class="custom-control custom-checkbox ">
                                        <input type="checkbox" class="custom-control-input" id="signup-terms" name="signup-terms">
                                        <label class="custom-control-label small mb-5" for="signup-terms">Съгласявам
                                            се с
                                            условията на сайта</label>
                                    </div>
                                    <hr>
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <i class="fa fa-plus mr-10"></i> Регистрация
                                    </button>

                                </div>

                            </div>
                        </div>
                        <div class="block-content bg-body-light">
                            <div class="form-group text-center">
                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="/usloviq"
                                   data-toggle="modal" data-target="#modal-terms">
                                    <i class="fa fa-book text-muted mr-5"></i> Условия и Правила
                                </a>
                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="/">
                                    <i class="fa fa-user text-muted mr-5"></i> Вход
                                </a>
                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="/">
                                    <i class="fa fa-user text-muted mr-5"></i> Забравена Парола
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END Sign Up Form -->
            </div>
        </div>
    </div>
</div>
@endsection
