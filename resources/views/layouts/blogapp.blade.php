@include('_static.header')
<body>
<!-- Page Container -->


<div id="page-container" class=" enable-page-overlay side-scroll page-header-inverse main-content-fluid
sidebar-inverse side-trans-enabled ">
    <!-- Side Overlay-->
    <aside id="side-overlay">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow">
            <div class="content-header-section align-parent">
                <!-- Close Side Overlay -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary align-v-r" data-toggle="layout" data-action="side_overlay_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Side Overlay -->

                <!-- User Info -->
                <div class="content-header-item">
                    <a class="img-link mr-5" href="javascript:void(0)">
                        <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                    </a>
                    <a class="align-middle link-effect text-primary-dark font-w600"
                       href="javascript:void(0)">remindbg</a>
                </div>
                <!-- END User Info -->
            </div>
        </div>
        <!-- END Side Header -->

        <!-- Side Content -->
        <div class="content-side">
            <p>
                Нещо си, TODO
            </p>
        </div>
        <!-- END Side Content -->
    </aside>

<!--  Sidebar -->

    <!-- Header -->
@include('_static.topnav')
<!-- END Header -->

    <!-- Main Container -->
    <main id="main-container">
        <div class="row content">
            <div class="col-lg-9">
                @yield('content')

            </div>
            <div class="col-lg-3 ">
                @include('components.blogsidebar')


            </div>


        </div>

    </main>
    <!-- END Main Container -->

@include('_static.footer')
