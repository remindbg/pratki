@extends('layouts.app')

@section('content')


    <!-- Page Content -->
    <!-- User Info -->



    <div class="test text-center">
        <h2 class="content-heading ">
            <i class="fa fa-key mr-5"></i> Security Кодове<i class="fa fa-key ml-5"></i>
        </h2>

        <div class="row">
            <div class="col-lg-12">
               <p>От тук можете да видите кодовете за верификация, които сте генерирали.
                   Кодовете се генерират автоматично и представляват 6 цифри от 0 до
                   9 като всеки код е свързан с потребителят, който го генерира.</p>
                <hr>
                <h3 class="text-primary">Имате Общо: <mark>{{ $tokens->count()}}</mark> кода</h3>
                <hr>
                <table class="table table-striped table-borderless table-hover table-vcenter">
                    <thead class="thead-light">
                    <tr>
                        <th class="text-center" style="width: 70px;"><i class="si si-key"></i></th>

                        <th class="">Създаден</th>
                        <th class="" style="width: 15%;">Активен?</th>

                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($tokens->sortByDesc('created_at')  as $token)
                        <tr class="{{$token->is_used ? 'text-danger' : 'text-primary'}}">

                            <td class="text-center">
                              <h3><mark>{{$token->code}}</mark></h3>
                            </td>
                            <td class="font-w600">
                                {{$token->created_at->diffForHumans()}}
                            </td>
                            <td class="">
                               @if ($token->is_used)
                                   Не
                                   @else
                                   Да
                               @endif
                            </td>

                        </tr>
                        <!-- END From Left Modal -->
                    @empty
                        <h4>Няма Открити Резултати</h4>
                    @endforelse


                    </tbody>
                </table>
                <a href="{{route('securetoken.create')}}"
                   class="btn btn-danger btn-block">Генериране на Нов Код
                </a>
            </div>
        </div>
    </div>



    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
