@extends('layouts.app')

@section('content')


    <!-- Page Content -->
    <!-- User Info -->



    <div class="test text-center">
        <h4 class="content-heading ">
            <i class="fa fa-key mr-5"></i> Успешно Генериран Код<i class="fa fa-key ml-5"></i>
        </h4>

        <div class="row">
            <div class="col-lg-12">

                <hr>
                <h1 class="text-primary">Вашият код е:  <mark>{{$token->code}}</mark></h1>
                <hr>
                <a href="{{route('securetoken.index')}}" class="btn btn-info btn-block"><i class="fa fa-backward mr-2
"></i>Назад</a>

            </div>
        </div>
    </div>



    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
