@extends('layouts.app')

@section('content')


    <!-- Page Content -->
    <!-- User Info -->




    <div class="test text-center">
        <h2 class="content-heading ">
            <i class="fa fa-key mr-5"></i> Генериране на Security Код
        </h2>

        <div class="row">
            <div class="col-lg-12">


                    <hr>
                    <form action="{{route('securetoken.store')}}" method="POST">
                        @csrf
                        <button class="btn btn-primary btn-block "><i class="fa fa-keybase"></i>Генериране на Код</button>
                    </form>



            </div>
        </div>
    </div>



    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
