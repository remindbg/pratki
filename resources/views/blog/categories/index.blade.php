<div class="content-header">
    <!-- Left Section -->
    <div class="content-header-section">

        <!-- Logo -->
        <div class="content-header-item">
            <a class="link-effect font-w700 mr-5" href="/">
                <i class="si si-fire text-primary"></i>
                <span class="font-size-xl text-dual-primary-dark">Check</span><span class="font-size-xl
                        text-primary">BG</span>
            </a>
        </div>
        <!-- END Logo -->

        <!-- Toggle Sidebar -->
        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
        @auth

        @endauth


        <div class="btn-group" role="group">
            <button type="button" class="btn btn-circle btn-dual-secondary" id="page-header-options-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-paint-brush"></i>
            </button>
            <div class="dropdown-menu min-width-300" aria-labelledby="page-header-options-dropdown">
                <h5 class="h6 text-center py-10 mb-10 border-b text-uppercase">Настройки</h5>
                <h6 class="dropdown-header">Цветова Гама</h6>
                <div class="row no-gutters text-center mb-5">
                    <div class="col-2 mb-5">
                        <a class="text-default" data-toggle="theme" data-theme="default" href="javascript:void(0)">
                            <i class="fa fa-2x fa-circle"></i>
                        </a>
                    </div>
                    <div class="col-2 mb-5">
                        <a class="text-elegance" data-toggle="theme" data-theme="{{ mix('/css/themes/elegance.css') }}" href="javascript:void(0)">
                            <i class="fa fa-2x fa-circle"></i>
                        </a>
                    </div>
                    <div class="col-2 mb-5">
                        <a class="text-pulse" data-toggle="theme" data-theme="{{ mix('/css/themes/pulse.css') }}" href="javascript:void(0)">
                            <i class="fa fa-2x fa-circle"></i>
                        </a>
                    </div>
                    <div class="col-2 mb-5">
                        <a class="text-flat" data-toggle="theme" data-theme="{{ mix('/css/themes/flat.css') }}" href="javascript:void(0)">
                            <i class="fa fa-2x fa-circle"></i>
                        </a>
                    </div>
                    <div class="col-2 mb-5">
                        <a class="text-corporate" data-toggle="theme" data-theme="{{ mix('/css/themes/corporate.css') }}" href="javascript:void(0)">
                            <i class="fa fa-2x fa-circle"></i>
                        </a>
                    </div>
                    <div class="col-2 mb-5">
                        <a class="text-earth" data-toggle="theme" data-theme="{{ mix('/css/themes/earth.css') }}" href="javascript:void(0)">
                            <i class="fa fa-2x fa-circle"></i>
                        </a>
                    </div>
                </div>
            </div>



            <!-- END Layout Options -->

        </div>
        @if (auth()->check() && auth()->user()->is_admin == true)
            <button type="button" class="btn  btn-dual-secondary">
                <a href="/panel" target="_blank"> <i class="fa fa-user-secret fa-fw"></i>Admin Panel</a>
            </button>
            <button type="button" class="btn  btn-dual-secondary">
                <a href="/search" target=""> <i class="fa fa-search fa-fw"></i>Търсене</a>
            </button>
        @else
            <button type="button" class="btn  btn-dual-secondary">
                <a href="/search" target=""> <i class="fa fa-search fa-fw"></i>Търсене</a>
            </button>
        @endif

        <button type="button" class="btn  btn-dual-secondary">
            <a href="/ranks" target=""> <i class="fa fa-trophy fa-fw"></i>Класация</a>
        </button>
    </div>

    <!-- END Left Section -->

    <!-- Right Section -->
    <div class="content-header-section">
        <!-- User Dropdown -->
        @auth
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user d-sm-none"></i>
                    <span class="d-none d-sm-inline-block">{{Auth::user()->getusername()}}</span>
                    <i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase"></h5>
                    <a class="dropdown-item" href="{{route('users.single',Auth::user()->id)}}">
                        <i class="si si-user mr-5"></i> Профил
                    </a>


                    <div class="dropdown-divider"></div>


                    <a class="dropdown-item" href="{{route('users.edit',Auth::user()->id)}}" >
                        <i class="si si-wrench mr-5"></i> Редакция
                    </a>
                    <!-- END Side Overlay -->

                    <div class="dropdown-divider"></div>
                    <form action="{{route('logout')}}" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item">
                            <i class="si si-logout mr-5"></i> Изход
                        </button>
                    </form>

                </div>
            </div>

        @endauth

        @guest
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user d-sm-none"></i>
                    <span class="d-none d-sm-inline-block">Профил</span>
                    <i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Профил</h5>
                    <a class="dropdown-item" href="/login">
                        <i class="si si-user mr-5"></i> Вход
                    </a>
                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="/register">
                        <span><i class="si si-envelope-open mr-5"></i> Регистрация</span>
                    </a>


                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/forgotpassword">
                        <i class="si si-user-unfollow mr-5"></i> Забравена Парола
                    </a>
                </div>
            </div>
        @endguest

        <div class="btn-group" role="group">
            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-info d-sm-none"></i>
                <span class="d-none d-sm-inline-block">Репутации</span>
                <i class="fa fa-angle-down ml-5"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Панел</h5>
                <a class="dropdown-item" href="javascript:void(0)">
                    <i class="fa fa-fw fa-reply mr-5"></i> Търсене
                </a>
                <a class="dropdown-item d-flex align-items-center justify-content-between" href="/register">
                    <span><i class="fa fa-info fa-fw mr-5"></i>Моите Репутации</span>

                </a>


            </div>
        </div>

    </div>
    <!-- END Right Section -->
</div>
