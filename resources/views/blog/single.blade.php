@extends('layouts.blogapp')


@section('seo')
    <title>{{$article->title}}</title>

    <meta name="description" content="Публикации и Статии">
@endsection


@section('content')


    <!-- Hero -->
    <div class="bg-primary">
        <div class="bg-pattern bg-black-op-25" style="background-image: url('{{asset('media/various/bg-pattern.png')}}');">
            <div class="content text-center">
                <div class="py-50">
                    <h1 class="font-w700 text-white mb-10">{{$article->title}}</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Blog and Sidebar -->
    <div class="content">
        <div class="content content-full nice-copy-story">
            <div class="row justify-content-center py-30">
                <div class="col-lg-8">
                    <a class="img-link" href="">
                        <img class="img-fluid" src="{{asset('uploads/'.$article->image_path)}}" alt="">
                    </a>
                    <h3>{{$article->title}}</h3>
                    <div class="text-muted mb-10">
                                    <span class="mr-15">
                                        <i class="fa fa-fw fa-calendar
                                        mr-5"></i>{{$article->created_at->diffForHumans()}}
                                    </span>

                        <a class="text-muted" href="{{$article->category->urlslug()}}">
                            <i class="fa fa-fw fa-tag mr-5"></i>{{$article->category->name}}
                        </a>
                        <a class="text-muted" href="{{$article->category->urlslug()}}">
                            <i class="fa fa-fw fa-eye mr-5"></i>{{$article->views}}
                        </a>
                    </div>
                    <p>
                        {!! $article->body !!}
                    </p>
                </div>
            </div>

        </div>



        <!-- END Posts -->

            <!-- Sidebar -->


            <!-- END Sidebar -->

    </div>



@endsection
