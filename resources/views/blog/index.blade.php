@extends('layouts.blogapp')


@section('seo')
    <title>Публикации и Статии</title>

    <meta name="description" content="Публикации и Статии">
@endsection


@section('content')


        <!-- Hero -->
        <div class="bg-primary">
            <div class="bg-pattern bg-black-op-25" style="background-image: url('{{asset('media/various/bg-pattern.png')}}');">
                <div class="content text-center">
                    <div class="py-50">
                        <h1 class="font-w700 text-white mb-10">Статии</h1>
                        <h2 class="h4 font-w400 text-white-op">Статии и Новини</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Blog and Sidebar -->
        <div class="content">
            <div class="">
                <!-- Posts -->

                @forelse($articles as $article)

                    <div class="col-xl-12">

                        <div class="mb-50">
                            <div class="overflow-hidden rounded mb-20" style="height: 200px;">
                                <a class="img-link" href="{{$article->urlslug()}}">
                                    <img class="img-fluid" src="{{asset('uploads/'.$article->image_path)}}" alt="">
                                </a>
                            </div>
                            <h3 class="h4 font-w700 text-uppercase mb-5">
                                <a href="{{$article->urlslug()}}">{{$article->title}}</a>
                            </h3>
                            <div class="text-muted mb-10">
                                    <span class="mr-15">
                                        <i class="fa fa-fw fa-calendar
                                        mr-5"></i>{{$article->created_at->diffForHumans()}}
                                    </span>

                                <a class="text-muted" href="javascript:void(0)">
                                    <i class="fa fa-fw fa-tag mr-5"></i>{{$article->category->name}}
                                </a>
                            </div>
                            <p>
                                {!! \Illuminate\Support\Str::limit($article->body,200) !!}
                            </p>

                        </div>


                        <hr class="d-xl-none">
                    </div>

                @empty


                @endforelse

                <!-- END Posts -->

                <!-- Sidebar -->


                <!-- END Sidebar -->
            </div>
        </div>



    @endsection
