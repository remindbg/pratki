@extends('admin.layouts.app')

@section('title','Преглед на Ип Адрес')

@section('content')

    <h5 class="mb-2">Преглед на IP Адрес: {{$ip->ipadress}}</h5>

    <div class="card-body">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Потребител</th>
                <th scope="col">Дата</th>
                <th>Инфо</th>
            </tr>
            </thead>
            <tbody>
            @forelse($ip->users as $user)

                <tr>

                    <td>{{$user->name}}</td>
                    <td>{{$user->created_at}}</td>
                    <td><a href="{{route('admin.users.edit',$user->id)}}"></a>Виж</td>
                </tr>

            @empty

                Няма Информация

            @endforelse

            </tbody>
        </table>


    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            })});


    </script>
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({selector:'#body'});</script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>
        .select2-results__option[aria-selected=true] {
            display: none;
        }
    </style>

@endsection
