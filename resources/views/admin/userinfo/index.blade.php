@extends('admin.layouts.app')
@section('title','Всички IP Адреси')

@section('content')
    <div class="card-body  p-0">

        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>IP Adres</th>


                <th>Последна Дата</th>
                <th>Потребители:</th>
                <th>Опции</th>
            </tr>
            @forelse($userinfo as $info)
                <tr class="">
                    <td>{{$info->id}}</td>
                    <td>{{$info->ipadress}}</td>

                    <td>{{$info->created_at->diffForHumans()}}</td>
                    <td>{{$info->users->ip_logins->count()}}</td>


                    <td>
                        <a href="{{route('admin.userinfo.show', $info->id)}}"><button class="btn
                        btn-primary
                        btn-sm">Преглед</button></a>

                    </td>
                </tr>

            @empty
                <hr>
                Няма Информация Все още.
            @endforelse
            </tbody>
        </table>
        <hr>
        {{$userinfo->links()}}
    </div>

    <!-- /.modal -->
@stop
