@extends('admin.layouts.app')

@section('title','Редакция на Категория')

@section('content')

    <h5 class="mb-2">Редакция на : {{$category->name}}</h5>

    <div class="card-body">
        <form action="{{route('admin.articlecategory.update',$category->id)}}" method="POST"
              enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-7">
                    @method('put')

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Обща Информация</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <small>Заглавие</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$category->name}}"
                                               name="name">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p>SLUG ( автоматично се създава от заглавието- после може да се редактира )</p>
                                    <div class="input-group mb-3">

                                        <input type="text" class="form-control" value="{{$category->slug}}"
                                               name="slug">

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Под Категория?</label>
                                                <select class="form-control" name="parent_id">
                                                    <option value="0" selected>Няма</option>
                                                    @forelse($jokecategories as $cat)
                                                        <option value="{{$cat->id}}"
                                                            {{$category->id == $cat->id ?
                                                            'selected' : ''}}>{{$cat->name}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>Изображение?</label>
                                                <select class="form-control" name="is_image">
                                                    <option value="0" {{$category->is_image ? '' : 'selected'}}
                                                    selected>Не</option>
                                                    <option value="1" {{$category->is_image ? 'selected' : ''}}>Да</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Изображение </label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="image"
                                                               id="exampleInputFile">
                                                        <label class="custom-file-label" for="exampleInputFile">Файл</label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Описание * <span class="small">Излиза отгоре под
                                                        името на категорията</span></label>
                                                <textarea class="form-control" rows="3" name="description" id="body"
                                                >{{$category->description}}</textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Редакция</button>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card card-blue collapsed-card">
                        <div class="card-header ">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">SEO Информация</h3>
                        </div>
                        <div class="card-body">


                            <div class="form-group">
                                <label>SEO Заглавие</label>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" value="{{$category->seo_title}}"
                                       name="seo_title"
                                >
                            </div>

                            <div class="form-group">
                                <label>SEO Описание</label>
                                <textarea class="form-control" rows="3"
                                          name="seo_description">{{$category->seo_description}}</textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>

    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            })});
    </script>
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({selector:'#body'});</script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

@endsection
