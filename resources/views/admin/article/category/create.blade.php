@extends('admin.layouts.app')

@section('title','Добавяне на Категория')

@section('content')

    <h5 class="mb-2">Нова Категория</h5>

    <div class="card-body">
        <form action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-7">
                    @method('post')

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Обща Информация</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <small>Заглавие</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value=""
                                               name="name">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p>SLUG - после може да се редактира </p>
                                    <div class="input-group mb-3">


                                        <input type="text" class="form-control" value=""
                                               name="slug" disabled>

                                    </div>
                                </div>


                            </div>

                        </div>
                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Създаване</button>
                    </div>
                </div>
            </div>
        </form>

    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            })});


    </script>
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({selector:'#body'});</script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

@endsection
