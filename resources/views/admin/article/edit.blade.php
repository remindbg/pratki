@extends('admin.layouts.app')

@section('title','Добавяне на нова Статия')

@section('content')

    <h5 class="mb-2">Нова Статия</h5>

    <div class="card-body">
        <form action="{{route('admin.articles.update',$article->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    @method('put')


                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Обща Информация</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <small>Заглавие</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$article->title}}"
                                               name="title">

                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <p>SLUG  </p>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$article->slug}}"
                                               name="slug">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Категория / задължително е да има категория заради
                                                    slug-a който depend-ва на категориите!</label>
                                                <select class="form-control" name="category_id">
                                                    @forelse($categories as $cat)
                                                        <option value="{{$cat->id}}"
                                                            {{$cat->id == $article->category->id ? 'selected' : ''
                                                            }}>{{$cat->name}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Текст<span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="22" cols="10" name="body"
                                                          id="body">{{$article->body}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Заглавно Изобр?</label>
                                <select class="form-control" name="is_image">
                                    <option value="0" selected>Не</option>
                                    <option value="1">Да</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="exampleInputFile">Смяна на Изображение</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="image"
                                               id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Файл</label>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Редакция на Статия</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            });
            $('#tags').select2({
                theme: 'bootstrap4',
                tags: true,
                // data: ["Clare","Cork","South Dublin"],
                tokenSeparators: [','],

                /* the next 2 lines make sure the user can click away after typing and not lose the new tag */

            });
        });



    </script>
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 500,
            menubar: true,
            plugins: [
                'advlist autolink lists link  charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount codesample', 'code image'
            ],
            toolbar: 'image code | codesample | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'],




        });
        let img = tinymce.selection.getNode();


    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>
        .select2-results__option[aria-selected=true] {
            display: none;
        }
    </style>

@endsection
