@extends('admin.layouts.app')
@section('title','Статии')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.articles.create')}}">
            <button type="button" class="btn btn-info btn-sm">Добавяне на Нова Статия</button>
        </a>
        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Име</th>

                <th> Категория</th>
                <th>Прегледа</th>
                <th>Дата</th>
                <th>Опции</th>
            </tr>
            @forelse($articles as $article)
                <tr class="">
                    <td>{{$article->id}}</td>
                    <td>{{\Illuminate\Support\Str::limit($article->title,22)}}</td>

                    <td>{{$article->category ? $article->category->name : 'НЯМА'}}</td>
                    <td>
                        {{$article->views}}
                    </td>
                    <td>{{$article->created_at}}</td>


                    <td>
                        <a href="{{route('admin.articles.edit', $article->id)}}"><button class="btn
                        btn-primary
                        btn-sm">Редакция</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                data-target="#modal-danger-{{$article->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$article->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Статия?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.articles.destroy',$article->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма добавени Статии Все още.
            @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.modal -->
@stop
