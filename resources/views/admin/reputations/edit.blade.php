@extends('admin.layouts.app')

@section('title','Редакция на Репутация')

@section('content')

    <h5 class="mb-2">Редакция на Репутация: {{$reputation->id}}</h5>

    <div class="card-body">
        <form action="{{route('admin.reputations.update',$reputation->id)}}" method="POST"
              enctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-12">
                    @method('put')
                    @csrf

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Обща Информация</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Активна?</p>
                                    <div class="form-group">

                                        <select class="form-control" name="is_active">

                                            <option value="1" {{$reputation->is_active ? 'selected' : ''}}>Да</option>
                                            <option value="0"  {{$reputation->is_active ? '' : 'selected'}}>Не</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <small>Получател {!! $reputation->receiver->textrepcount() !!}</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$reputation->receiver->name}}"
                                               name="receiver" disabled>

                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <small>Изпращач {!! $reputation->sender->textrepcount() !!}</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$reputation->sender->name}}"
                                               name="sender" disabled>

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <small>Номер На Пратка</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$reputation->trackingcode}}"
                                               name="trackingcode">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p>Репутация Тип:</p>
                                    <div class="form-group">

                                        <select class="form-control" name="is_negative">

                                            <option value="1" {{$reputation->is_negative ?
                                            'selected' : ''}}>Отрицателна</option>
                                            <option value="0" {{$reputation->is_negative ? '' :
                                            'selected'}}>Положителна</option>

                                        </select>
                                    </div>
                                </div>

                                </div>

                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Редакция</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            })});


    </script>
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({selector:'#body'});</script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>
        .select2-results__option[aria-selected=true] {
            display: none;
        }
    </style>

@endsection
