@extends('admin.layouts.app')
@section('title','Всички Потребители')

@section('content')
    <div class="card-body  p-0">

        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>дата</th>

                <th>От</th>
                <th>Към</th>
                <th>Тип</th>



                <th>Активна?</th>
                <th>Опции</th>
            </tr>
            @forelse($reputations as $reputation)
                <tr class="{{$reputation->is_active ? '' : 'text-warning'}}">
                    <td>{{$reputation->id}}</td>
                    <td>{{$reputation->created_at}}</td>

                    <td>
                        <a href="{{route('admin.users.edit',$reputation->sender->id)}}"
                           target="_blank">{{$reputation->sender->name}} / {!! $reputation->sender->textrepcount() !!}</a>
                    </td>
                    <td>
                        <a href="{{route('admin.users.edit',$reputation->receiver->id)}}"
                           target="_blank">{{$reputation->receiver->name}}/ {!! $reputation->receiver->textrepcount()
                           !!}</a>
                    </td>

                    @if ($reputation->is_negative)
                        <td>Отрицателна</td>

                    @else
                        <td>Положителна</td>
                    @endif
                    @if ($reputation->is_active)
                        <td>Да</td>

                    @else
                        <td>Не</td>
                    @endif
                    <td>
                        <a href="{{route('admin.reputations.edit', $reputation->id)}}"><button class="btn
                        btn-primary
                        btn-sm">Редакция</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$reputation->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$reputation->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Репутация?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.reputations.destroy',$reputation->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма Репутации Все още.
            @endforelse
            </tbody>
        </table>
        <hr>
        {{$reputations->links()}}
    </div>

    <!-- /.modal -->
@stop
