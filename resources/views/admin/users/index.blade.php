@extends('admin.layouts.app')
@section('title','Всички Потребители')

@section('content')
    <div class="card-body  p-0">

        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>username</th>

                <th>email</th>
                <th>телефон</th>
                <th>Рейтинг</th>

                <th>Регистрация</th>

                <th>Админ?</th>
                <th>Опции</th>
            </tr>
            @forelse($users as $user)
                <tr class="{{$user->is_active ? '' : 'bg-warning'}}">
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>

                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{!! $user->textrepcount() !!}</td>

                    <td>{{$user->created_at}}</td>
                    @if ($user->is_admin)
                        <td>Да</td>

                    @else
                        <td>Не</td>
                    @endif

                    <td>
                        <a href="{{route('admin.users.edit', $user->id)}}"><button class="btn
                        btn-primary
                        btn-sm">Редакция</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-danger-{{$user->id}}">
                            Изтриване
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$user->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Изтриване на Потребител?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.users.destroy',$user->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Изтриване</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Отказ</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                Няма потребители Все още.
            @endforelse
            </tbody>
        </table>
        <hr>
        {{$users->links()}}
    </div>

    <!-- /.modal -->
@stop
