@extends('admin.layouts.app')

@section('title','Редакция на Потребител')

@section('content')

    <h5 class="mb-2">Редакция на Потребител: {{$user->username}}</h5>

    <div class="card-body">
        <form action="{{route('admin.users.update',$user->id)}}" method="POST" enctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-7">
                    @method('put')
                    @csrf

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Обща Информация</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Потребителя е търговец?</p>
                                    <div class="form-group">

                                        <select class="form-control" name="is_merchant">

                                            <option value="0" selected>Не</option>
                                            <option value="1">Търговец</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <small>Email</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$user->email}}"
                                               name="email">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <small>Username</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$user->name}}"
                                               name="name">

                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <small> Име</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$user->firstname}}"
                                               name="firstname">

                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <small>Фамилия</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$user->lastname}}"
                                               name="lastname">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <small>Телефон</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$user->phone}}"
                                               name="phone">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p>Aктивен?</p>
                                    <div class="form-group">

                                        <select class="form-control" name="is_active">

                                            <option value="1" {{$user->is_active ? 'selected'
                                                : ''}}>Активен</option>
                                            <option value="0" {{$user->is_active ? '' :
                                                'selected'}}>Неактивен</option>

                                        </select>
                                    </div>
                                </div>
                                @if($user->avatar_path)
                                    <div class="col-lg-12">
                                        <p>Изтриване на Аватара</p>
                                        <div class="form-group">

                                            <select class="form-control" name="delete_avatar">

                                                <option value="0" selected>Не</option>
                                                <option value="1">Да</option>

                                            </select>
                                        </div>
                                    </div>
                                    @else
                                    Потребителят Няма Аватар
                                @endif

                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Редакция</button>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card card-blue collapsed-card">
                        <div class="card-header ">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Администратор?</h3>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label>Администратор?</label>
                            </div>
                            <div class="input-group mb-3">
                                <select class="form-control" name="is_admin">

                                        <option value="1" {{$user->is_admin ? 'selected' : ''}}>Да</option>
                                    <option value="0" {{$user->is_admin ? '' : 'selected'}}>Не</option>

                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="card card-danger collapsed-card">
                        <div class="card-header ">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">IP Aдреси</h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">IP Адрес</th>
                                    <th scope="col">Дата</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($user->ip_logins as $ip)

                                    <tr>

                                        <td>{{$ip->ipadress}}</td>
                                        <td>{{$ip->created_at}}</td>
                                    </tr>

                                @empty

                                    Няма Информация

                                @endforelse

                                </tbody>
                            </table>




                        </div>
                    </div>
                    <div class="card card-success collapsed-card">
                        <div class="card-header ">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Репутации</h3>
                        </div>
                        <div class="card-body">




                        </div>
                    </div>

                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            })});


    </script>
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({selector:'#body'});</script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>
        .select2-results__option[aria-selected=true] {
            display: none;
        }
    </style>

@endsection
