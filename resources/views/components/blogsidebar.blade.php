<hr class="mb-200">
<div class="block block-transparent">
    <div class="block-header">
        <h4 class=" text-uppercase text-primary">
            <i class="fa fa-fw fa-list mr-5"></i> Категории
        </h4>
    </div>
    <div class="block-content">
        <ul class="nav nav-pills flex-column push">
            @forelse($categories as $category2)

                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center justify-content-between" href="javascript:void(0)">
                        <span><i class="fa fa-fw fa-newspaper-o mr-2"></i> {{$category2->name}}</span>
                        <span class="badge badge-pill badge-secondary mr-2">{{$category2->articles->count()}}</span>
                    </a>
                </li>


            @empty

            @endforelse

        </ul>
    </div>
</div>

    <!-- Twitter Feed -->
    <div class="block block-transparent">
        <div class="block-header">
            <h4 class="text-uppercase text-primary">Последни Статии</h4>

        </div>
        <div class="block-content">



                @forelse($recentarticles as $article)


                <div class="mb-20">
                    <img class="img-link img-thumbnail" src="{{asset('uploads/'.$article->image_path)}}"
                     alt="">


                        <h5 class="mb-1 text-primary">{{$article->title}} </h5>
                        <div class="font-size-sm text-muted">{{$article->created_at->diffForHumans()}}</div>

                </div>
                <hr>

                @empty

            @endforelse


        </div>
    </div>

