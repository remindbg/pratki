@auth
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Нотификации</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
            </div>
        </div>
        <div class="block-content">
            <ul class="list list-activity">
                {{--            {{dd($unseen)}}--}}
                @forelse($unseen as $rep)

                    <li class="{{$rep->is_negative ? 'text-danger' : 'text-primary'}}">
                        <i class="si si-user-follow "></i>
                        <div class="font-w600">
                            <a href="{{route('users.single',$rep->sender->id)}}" target="_blank">{{$rep->sender->name}}</a> ви остави
                            {{$rep->is_negative ? 'Отрицателна' : 'Положителна'}} репутация.
                        </div>
                        <div class="text-right">
                            <a href="{{route('reputation.single',$rep->id)}}" class="text-bold btn-primary
                    btn-sm">Разгледайте</a>
                        </div>
                        <div class="font-size-xs text-muted">{{$rep->created_at->diffForHumans()}}</div>
                    </li>
                    <hr>


                @empty
                    Нямате нови нотификации
                @endforelse

            </ul>
        </div>
    </div>
    @endauth

<div class="block ">
    <div class="block-header block-header-default">
        <h3 class="block-title">Последни Потребители</h3>
        <div class="block-options">
            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                <i class="si si-refresh"></i>
            </button>
            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
        </div>
    </div>
    @forelse ($users as $user)
        <div class="block-content">
            <div class="media mb-1">
                <img class="img-avatar img-avatar32 d-flex mr-20" src="{{$user->avatar_path ? asset('uploads/'
                .$user->avatar_path) : asset('avatar.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-1"><a class="text-primary" href="{{route('users.single',$user->id)
                    }}">{{$user->name}}</a> се
                        регистрира

                    <div class="text-muted">{{$user->created_at->diffForHumans()}}</div>
                </div>
            </div>

        </div>
    @empty
        Няма Потребители

    @endforelse


</div>
<!-- END Twitter Feed -->

<!-- Categories -->
<div class="block block-transparent">
    <div class="block-header">
        <h3 class="block-title text-uppercase text-primary">
            <i class="fa fa-fw fa-list mr-5"></i> Други Страници
        </h3>
    </div>
    <div class="block-content">
        <ul class="nav nav-pills flex-column push">
            <li class="nav-item">
                <a class="nav-link d-flex align-items-center justify-content-between" href="{{route('static.faq')}}">
                    <span><i class="fa fa-fw fa-question mr-5"></i> ЧЗВ</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link d-flex align-items-center justify-content-between" href="{{route('static.usloviq')}}">
                    <span><i class="fa fa-fw fa-magic mr-5"></i>Условия</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link d-flex align-items-center justify-content-between" href="{{route('static.contact')
                }}">
                    <span><i class="fa fa-fw fa-envelope mr-5"></i> Контакти</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link d-flex align-items-center justify-content-between" href="{{route('articles.index')}}">
                    <span><i class="fa fa-fw fa-book mr-5"></i> Статии и Новини</span>
                </a>
            </li>

        </ul>
    </div>
</div>
