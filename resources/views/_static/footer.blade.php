
<!-- Footer -->
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <div class="float-right">
            Всички Права Запазени <i class="fa fa-heart text-pulse"></i> от
            <a class="font-w600" href="#" target="_blank">CheckerBG</a>
        </div>
        <div class="float-left">
            <a class="font-w600" href="#" target="_blank">Checkerbg</a> &copy;
            <span class="js-year-copy"></span>
        </div>
    </div>
</footer>
<!-- END Footer -->
</div>
<!-- END  Container -->

<!--  JS -->
<script src="{{ asset('build/app.min.js') }}"></script>

@yield('js_after')
</body>
</html>
