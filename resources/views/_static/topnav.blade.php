<header id="page-header">

    <div class="content-header">

        <div class="content-header-section">

            <div class="content-header-item mr-2">
                <a class="link-effect font-w600 hover" href="/">
                    <span class="text-dual-primary-dark">Check</span><span class="text-primary">BG</span>
                </a>

            </div>
            <div class="content-header-item">
                <ul class="nav-main-header">
                    <li>
                        <a class="{{request()->is('search') ? 'active' : ''}}" href="{{route('search.index')}}"><i
                                class="fa
                        fa-search"></i>Търсене</a>
                    </li>


                </ul>
            </div>

        </div>

        <div class="content-header-section">

            <ul class="nav-main-header">
                @guest
                    <li>
                        <a href="{{route('login')}}">Вход</a>
                    </li>
                    <li>
                        <a href="{{route('register')}}">Регистрация</a>
                    </li>
                @endguest


                    @auth
                        @if (auth()->user()->is_admin)
                            <li>
                                <a class="" href="/panel"><i class="si
                    si-user-follow"></i>Admin Panel</a>
                            </li>

                        @endif
                        <li>
                            <a class="" target="_blank" href="{{route('users.single',auth()->user()->id)}}"><i class="si
                    si-user"></i>{{auth()->user()->name}}</a>
                        </li>
                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si
                    si-settings"></i>Настройки</a>
                            <ul>
                                <li>
                                    <a href="{{route('users.edit',auth()->user()->id)}}">Редакция</a>
                                </li>
                                <li>
                                    <a href="{{route('securetoken.index')}}">Кодове</a>
                                </li>
                                <li>
                                    <a href="{{route('logout')}}" onclick="return confirm('Изход?')">Изход</a>
                                </li>

                            </ul>
                        </li>
                        @endauth


            </ul>

            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                <i class="fa fa-navicon"></i>
            </button>

        </div>
    </div>


    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>

</header>
<nav id="sidebar">
    <div class="sidebar-content">
        <div class="content-header content-header-fullrow bg-black-op-10">
            <div class="content-header-section text-center align-parent">

                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>

                <div class="content-header-item">
                    <a class="link-effect font-w700" href="/">
                        <i class="si si-fire text-primary"></i>
                        <span class="font-size-xl text-dual-primary-dark">Check</span><span class="font-size-xl
                        text-primary">BG</span>
                    </a>
                </div>

            </div>
        </div>

        <div class="content-side content-side-full">

            <ul class="nav-main">
                <li>
                    <a class="{{request()->is('/') ? 'active' : ''}}" href="{{route('home')}}"><i
                            class="fa fa-home"></i>Начало</a>
                </li>
                <li>
                    <a class="{{request()->is('search') ? 'active' : ''}}" href="{{route('search.index')}}"><i
                            class="fa fa-search"></i>Търсене</a>
                </li>


                <li>
                    <a class="nav-submenu {{request()->is('reputations') ? 'active' : ''}}"
                       data-toggle="nav-submenu" href="#"><i class="si si-trophy"></i>Репутации</a>
                    <ul>
                        @guest
                            <li>
                                <a href="{{route('login')}}">Вход</a>
                            </li>
                            <li>
                                <a href="{{route('register')}}">Регистрация</a>
                            </li>
                        @endguest

                        @auth

                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">{{auth()->user()->name}}</a>
                                    <ul>
                                        <li>
                                            <a href="{{route('users.single',auth()->user()->id)}}">Публичен Профил</a>
                                        </li>
                                        <li>
                                            <a href="{{route('users.edit',auth()->user()->id)}}">Редакция на Профила</a>
                                        </li>
                                        <li>
                                            <a href="{{route('securetoken.index')}}">Кодове за Сигурност</a>
                                        </li>

                                        <li>
                                            <a href="{{route('logout')}}" onclick="return confirm('Изход?')">Изход</a>
                                        </li>
                                    </ul>
                                </li>
                         @endauth
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</nav>
