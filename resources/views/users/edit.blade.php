@extends('layouts.app')

@section('content')


    <!-- Page Content -->
    <!-- User Info -->


    <div class="bg-primary">
        <div class="content  text-center">
            <!-- Avatar -->
            <div class="mb-15">
                <a class="img-link" href="#">
                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{$user->avatar_path}}"
                         alt="">
                </a>
            </div>
            <!-- END Avatar -->

            <!-- Personal -->
            <h1 class="h1 text-white font-w700 mb-10">{{ucfirst($user->firstname)}} {{ucfirst($user->lastname)}}</h1>
            <h4 class="h5 text-white-op">
                @ {{$user->name}}
            </h4>
            <!-- END Personal -->

            <!-- Actions -->


            <!-- END Actions -->

            <p class=" text-white-op">
                {{$user->about}}
            </p>
        </div>



    </div>

    <div class="content">
        <form class="js-validation-signup" action="{{route('users.update',$user->id)}}"
              method="POST"
              enctype="multipart/form-data" >
            @csrf
            <div class="block block-themed block-rounded block-shadow">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Редакция на Данни</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-user"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div>


                            <a href="{{route('securetoken.index')}}"
                               class="btn btn-danger btn-block"><i class="fa fa-key mr-2"></i>Кодове за Сигурност (
                                {{$user->tokens->count()}} налични )<i
                                    class="fa fa-key ml-2"></i>
                            </a>


                    </div>
                    <hr>
                    <div class="form-group row">

                        <div class="col-lg-6 col-xs-12">
                            <div class="input-group">
                                <label for="username">Потребителско Име</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                    </div>
                                    <input type="text" class="form-control" id="username"
                                           name="name" placeholder="" disabled value="{{$user->name}}">
                                </div>

                            </div>
                            <hr>

                        </div>
                        <div class="col-lg-6 col-xs-12">

                            <label for="phone">Телефон</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                </div>
                                <input type="text" class="form-control" id="phone"
                                       name="" value="{{$user->phone}}" disabled>
                            </div>
                            <hr>

                        </div>

                        <div class="col-lg-6 col-xs-12">

                            <label for="firstname">Име</label>
                            <div class="input-group">
                                <div class="input-group-prepend">

                                </div>
                                <input type="text" class="form-control" id="firstname"
                                       name="firstname"  disabled value="{{$user->firstname}}">
                            </div>

                        </div>

                        <div class="col-lg-6 col-xs-12">

                            <label for="lastname">Фамилия</label>
                            <div class="input-group">
                                <div class="input-group-prepend">

                                </div>
                                <input type="text" class="form-control" id="lastname"
                                       name="lastname" disabled value="{{$user->lastname}}">
                            </div>

                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="email">Валиден Имейл Адрес</label>
                            <input type="email" class="form-control" id="email"
                                   name="email"
                                   value="{{$user->email}}" disabled>
                        </div>
                        <hr>
                    </div>
                    <div class="form-group row">

                        <label class="col-12">Профлна Снимка <span class="small">Разрешени формати: .jpg
                                / jpeg / .png - Максимален Размер - 4MB</span></label>
                        <div class="col-12">
                            <div class="custom-file">
                                <!-- Populating custom file input label with the selected filename (data-toggle="custom-file-input" is initialized in Helpers.coreBootstrapCustomFileInput()) -->
                                <input type="file" class="custom-file-input" name="avatar">
                                <label class="custom-file-label" for="example-file-input-custom">Избиране</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="about">Кратко Описание</label>
                        <div class="col-12">
                            <textarea class="form-control"
                                      id="about" name="about" rows="4">{{$user->about}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-0">

                        <div class="col-sm-12 text-sm-left push">

                            <hr>
                            <button type="submit" class="btn btn-primary btn-block">
                                <i class="fa fa-edit mr-10"></i> Редакция
                            </button>

                        </div>

                    </div>
                </div>
                <div class="block-content bg-body-light">
                    <div class="form-group text-center">

                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{route('users.changepassword')}}">
                            <i class="fa fa-key text-muted mr-5"></i> Смяна на парола
                        </a>

                    </div>
                </div>
            </div>
        </form>

    </div>




    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
