@extends('layouts.app')


@section('seo')
    <title>Потребител: {{$user->name}}</title>

    <meta name="description" content="Информация за потребител: {{$user->name}}">
@endsection



@section('content')


    <!-- Page Content -->
    <!-- User Info -->


    <div class="bg-primary block block-content full ribbon ribbon-left ribbon-bookmark ribbon-crystal"
         style="background-image: url('{{asset('media/various/bg-pattern.png')}}');">
        <div class="content">
            <!-- Avatar -->
            <div class="mb-15  text-center">
                <a class="img-link" href="#">
                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{$user->avatar_path ? asset('uploads/'
                    .$user->avatar_path): asset('avatar.png')}}"
                         alt="">
                </a>
            </div>
            <!-- END Avatar -->
            <div class="ribbon-box">
                <i class="fa fa-check"></i>

                @if ($user->is_admin)
                    Администратор

                    @elseif($user->is_merchant)
                    Търговец
                    @else
                    Потребител
                @endif
            </div>
            <!-- Personal -->
            <h1 class="h1 text-white font-w700 mb-10  text-center">{{ucfirst($user->firstname)}} {{ucfirst($user->lastname)}}</h1>
            <h4 class="h5 text-white-op  text-center">
                @ {{$user->name}} :  {!! $user->textrepcount() !!}
            </h4>



            @auth
                @if ($user->id == auth()->user()->id)

                    <a  class="btn btn-block btn-rounded btn-info  btn-sm  mb-5"
                          href="{{route('users.edit',$user->id)}}">
                        <i class="fa fa-cog mr-2"></i>Редакция на Профила
                    </a>

                @else
                    @if ($user->is_merchant)
                        <a href="#" class="btn btn-block btn-rounded btn-hero btn-sm
                btn-alt-warning
                mb-5">
                            <i class="fa fa-plus mr-5"></i>Не можете да оставяте репутация на търговци
                        </a>
                    @else
                        <a href="{{route('reputation.create',$user->id)}}" class="btn btn-block btn-rounded btn-hero btn-sm
                btn-alt-success
                mb-5">
                            <i class="fa fa-plus mr-5"></i>Добавете  Репутация
                        </a>
                        <a href="{{route('users.check',$user->id)}}" class="btn btn-rounded btn-block btn-hero btn-sm
                    btn-alt-primary
                    mb-5">
                            <i class="fa fa-user-secret mr-5"></i>Проверка на Код
                        </a>
                    @endif


                @endif

            @endauth

            @guest

                <a href="{{route('reputation.create',$user->id)}}" class="btn btn-block btn-rounded btn-hero btn-sm
                btn-alt-success
                mb-5">
                    <i class="fa fa-plus mr-5"></i>Добавете  Репутация
                </a>
                <button type="button" class="btn btn-rounded btn-block btn-hero btn-sm btn-alt-primary mb-5">
                    <i class="fa fa-user-secret mr-5"></i>Проверка на Код
                </button>
            @endguest


            <!-- END Actions -->



            <p class=" text-white-op">
               {{$user->description}}
            </p>
        </div>



    </div>

    <div class="test">
        <h2 class="content-heading">
            <i class="si si-note mr-5"></i> Последно Получени Репутации
        </h2>
        <hr>
        @forelse($reputationreceived->where('is_active',true) as $reputation)
        <a class="block block-rounded block-link-shadow " href="{{route('users.single',$reputation->sender->id)}}">
            <div class="block-content block-content-full ">

                <p class="font-size-sm text-muted float-sm-right
                mb-5"><em>{{$reputation->created_at->diffForHumans()}}</em></p>
                <h4 class="font-size-default  mb-0 {{$reputation->is_negative ? 'text-danger' :
                'text-success'}}">
                    <i class="fa {{$reputation->is_negative ? 'fa-frown-o' : 'fa-smile-o'}} text-muted mr-5"></i>
                    {{$reputation->is_negative ? 'Отрицателна' : 'Положителна'}} Репутация от
                    @ {{$reputation->sender->name}}
                </h4>
            </div>
        </a>
        @empty
            @endforelse

        <hr>
    </div>





@stop
