@extends('layouts.app')

@section('content')


    <div class="content">
        <form class="js-validation-signup" action="{{route('users.updatepassword')}}"
              method="POST"
              enctype="multipart/form-data" >
            @csrf
            <div class="block block-themed block-rounded block-shadow">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Смяна на Парола</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-user"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">





                    </div>
                    <div class="form-group row">
                        <div class="col-12 text-center">
                            <label for="password">Въведете Нова Парола</label>
                            <input type="password" class="form-control" id="password"
                                   name="password"
                                   value="" >
                        </div>
                        <hr>
                    </div>



                    <div class="form-group row mb-0">

                        <div class="col-sm-12 text-sm-left push">

                            <hr>
                            <button type="submit" class="btn btn-primary btn-block">
                                <i class="fa fa-key mr-10"></i> Смяна на Парола
                            </button>

                        </div>

                    </div>
                </div>


        </form>

    </div>




    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
