@extends('layouts.app')

@section('content')


    <div class="test text-center">
        <h2 class="content-heading ">
            <i class="fa fa-truck mr-5"></i> Проверка за Валиден Код на  : <a href="{{route('users.single',
            $user->id)}}" target="_blank">{{$user->name}}</a>
        </h2>

        <div class="row">
            <div class="col-lg-12">

                <hr>

                <form action="{{route('users.checkpost',$user->id)}}" method="POST">
                    @csrf

                    <div class="form-group row">
                        <label class="col-lg-5" for="example-textarea-input">Код за сигурност</label>

                        <div class="col-lg-7 col-xs-12">
                            <input type="text" min="100000" max="999999"
                                   class="form-control form-control-lg"
                                   placeholder="6 Цифрен код"
                                   name="securecode">
                        </div>
                    </div>


                    <hr>

                    <button class="btn btn-success btn-block btn-lg" type="submit"><i class="fa fa-arrow-right
                    mr-2"></i>Проверка За Валидност</button>
                </form>


            </div>
        </div>
    </div>



    <!-- END User Info -->

    <!-- Main Content -->

    <!-- END Main Content -->
    <!-- END Page Content -->


@stop
