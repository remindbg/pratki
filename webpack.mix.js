const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    /* CSS */
    .sass('resources/sass/main.scss', 'public/build/style.css')
    .sass('resources/sass/codebase/themes/flat.scss', 'public/build/style.css')

    /* JS */


    .js('resources/js/app.js', 'public/build/app.min.js')
    .js('resources/js/codebase/app.js', 'public/build/app.min.js')

    /* Page JS */

    /* Tools */
    .browserSync('localhost:8000')
    .disableNotifications()

    /* Options */
    .options({
        processCssUrls: false
    });
